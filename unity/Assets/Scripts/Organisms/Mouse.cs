﻿using UnityEngine;

public class Mouse : Organism
{
    protected override string fullPath => Application.dataPath + "/BehaviorTrees/" + nameof(Mouse) + "BT.bt";
}