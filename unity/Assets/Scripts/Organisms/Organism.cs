﻿using System.Collections.Generic;
using AillieoUtils.EasyBehaviorTree;
using BehaviorTrees;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public abstract class Organism : MonoBehaviour, IBlackBoardData
{
    private BehaviorTree behaviorTree;


    public float speed = 1f;
    public int health = 500;
    private int healthCurrent;

    public Vector3 currentDirection = Vector3.zero;

    private SphereCollider sensor = null;

    private List<Organism> nearbyOrganisms = new List<Organism>();

    protected abstract string fullPath { get; }

    private void Start()
    {
        sensor = GetComponent<SphereCollider>();
    }

    void Update()
    {
        if (behaviorTree != null && Alive)
        {
            behaviorTree.Tick(Time.deltaTime);
        }
    }

    public void Restart()
    {
        healthCurrent = health;
        behaviorTree = BytesAssetProcessor.LoadBehaviorTree(fullPath);

        if (behaviorTree != null)
        {
            behaviorTree.Restart();
            behaviorTree.blackBoard[BlackboardKeys.SELF] = this;

            behaviorTree.OnBehaviorTreeCompleted += (bt, st) => bt.Restart();
        }
    }

    public void DecreaseHealth(int amount)
    {
        healthCurrent -= amount;

        if (Alive) return;

        if (behaviorTree != null)
        {
            behaviorTree.CleanUp();
            behaviorTree = null;
        }

        Destroy(gameObject, 1);
    }

    public bool Alive => healthCurrent >= 0;

    public Organism GetNearbyOrganism(out Organism organism)
    {
        return organism = nearbyOrganisms.Count <= 0 ? null : nearbyOrganisms[0];
    }

    public Vector3 PickRandomMoveDirection()
    {
        return currentDirection = new Vector3(Random.Range(-50, 50), Random.Range(-50, 50), 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.TryGetComponent(out Organism organism)) return;
        if (nearbyOrganisms.Contains(organism)) return;

        nearbyOrganisms.Add(organism);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.TryGetComponent(out Organism organism)) return;
        if (!nearbyOrganisms.Contains(organism)) return;

        nearbyOrganisms.Remove(organism);
    }
}