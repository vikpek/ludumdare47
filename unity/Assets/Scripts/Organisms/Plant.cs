﻿using UnityEngine;

public class Plant : Organism
{
    protected override string fullPath => Application.dataPath + "/BehaviorTrees/" + nameof(Plant) + "BT.bt";
}