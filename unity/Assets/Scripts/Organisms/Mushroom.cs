﻿using UnityEngine;

public class Mushroom : Organism
{
    protected override string fullPath => Application.dataPath + "/BehaviorTrees/" + nameof(Mushroom) + "BT.bt";
}