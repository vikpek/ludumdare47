﻿using UnityEngine;

public class BirdOfPrey : Organism
{
    protected override string fullPath => Application.dataPath + "/BehaviorTrees/" + nameof(BirdOfPrey) + "BT.bt";
}