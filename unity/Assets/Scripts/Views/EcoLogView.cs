﻿using System.Collections.Generic;
using ScriptableObjects;
using TMPro;
using UnityEngine;

    
    public class EcoLogView : MonoBehaviour
    {
        [SerializeField] private EcoLog ecosystemData = default;

        [SerializeField] private TextMeshProUGUI textEcoLog = default;
        [SerializeField] private TextMeshProUGUI textOxygenLog = default;
        private int index;

        private void Update()
        {
            textEcoLog.text = CreateTextViewFromList(ecosystemData.ecoMessages, 5);
            // textOxygenLog.text = ecosystemData.ecoOxygen[ecosystemData.ecoOxygen.Count].ToString();
        }

        private string CreateTextViewFromList(List<string> ecosystemDataEcoMessages, int logEntryCount)
        {
            string result = "";
            for (int i = 0; i < logEntryCount; i++)
            {
                index = ecosystemDataEcoMessages.Count - i;
                if (index >= 0 && index < ecosystemDataEcoMessages.Count)
                    result = result + "\n" + ecosystemDataEcoMessages[index];
            }
            return result;
        }
    }
