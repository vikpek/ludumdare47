using System.Collections;
using System.Collections.Generic;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject mouse = default;
    [SerializeField] private GameObject plant = default;
    [SerializeField] private GameObject birdOfPrey = default;
    [SerializeField] private GameObject mushroom = default;

    public EcoData eco = default;
    public EcoLog ecoLog = default;
    public EcoStatistics ecoStatistics = default;

    private static GameManager mInstance;

    private void Start()
    {
        SceneManager.LoadScene("UI", LoadSceneMode.Additive);
        StartCoroutine(InitiateDayNightCycle());
    }

    private IEnumerator InitiateDayNightCycle()
    {
        while (true)
        {
            CleanListOfNullEntries();
            
            switch (eco.daytime)
            {
                case Daytime.Day:
                    ExecuteDayStart();
                    yield return new WaitForSeconds(eco.durationDay);
                    ExecuteDayEnd();
                    eco.daytime = Daytime.Night;
                    break;
                case Daytime.Night:
                    ExecuteNightStart();
                    yield return new WaitForSeconds(eco.durationNight);
                    ExecuteNightEnd();
                    eco.daytime = Daytime.Day;
                    break;
            }
        }
    }

    private void CleanListOfNullEntries()
    {
        eco.organisms.RemoveAll(x => x == null);
    }

    private void ExecuteNightEnd()
    {
    }

    private void ExecuteNightStart()
    {
        ecoLog.ecoMessages.Add(EcoLogMessages.DAYTIME_NIGHT_START);
        SpawnBirdOfPrey();
        
        foreach (Organism organism in eco.organisms)
        {
            if (organism == null) continue;
            NightlyHealthDecrease(organism);
        }

        List<Organism> deathOrganisms = eco.organisms.FindAll(x => x.Alive == false);

        foreach (Organism deathOrganism in deathOrganisms)
        {
            SpawnMushroom(deathOrganism.transform.position);
            eco.organisms.Remove(deathOrganism);
        }
    }

    private void NightlyHealthDecrease(Organism organism)
    {
        organism.DecreaseHealth(eco.nightHealthLoss);
    }

    private void SpawnMushroom(Vector3 transformPosition)
    {
        Organism instantiatedOrganism = Instantiate(mushroom).GetComponent<Organism>();
        instantiatedOrganism.transform.position = transformPosition;
        Instantiation(instantiatedOrganism);
        ecoStatistics.countMushrooms++;
    }

    private void Instantiation(Organism instantiatedOrganism)
    {
        eco.organisms.Add(instantiatedOrganism);
        instantiatedOrganism.Restart();
    }

    private void SpawnBirdOfPrey()
    {
        Organism instantiatedOrganism = Instantiate(birdOfPrey).GetComponent<Organism>();
        Instantiation(instantiatedOrganism);
        ecoStatistics.countBirdsOfPrey++;
    }

    private void SpawnMouse()
    {
        Organism instantiatedOrganism = Instantiate(mouse).GetComponent<Organism>();
        Instantiation(instantiatedOrganism);
        ecoStatistics.countMouses++;
    }

    private void ExecuteDayEnd()
    {
    }

    private void ExecuteDayStart()
    {
        ecoLog.ecoMessages.Add(EcoLogMessages.DAYTIME_DAY_START);
        SpawnMouse();

    }
}