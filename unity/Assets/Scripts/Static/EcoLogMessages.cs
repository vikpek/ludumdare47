﻿    public static class EcoLogMessages
    {
        public static string DAYTIME_NIGHT_END = "Night has ended";
        public static string DAYTIME_DAY_END = "Day has ended";
        
        public static string DAYTIME_NIGHT_START = "Night has begun";
        public static string DAYTIME_DAY_START = "Day has begun";
    }
