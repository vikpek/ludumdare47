﻿using System;
using AillieoUtils.EasyBehaviorTree;
using BehaviorTrees;
using JetBrains.Annotations;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class PickRandomDirection : NodeAction
{
    public override void Cleanup()
    {
    }

    protected override BTState ExecuteTask(float deltaTime)
    {
        Organism self = behaviorTree.blackBoard[BlackboardKeys.SELF] as Organism;
        if (self == null) return BTState.Failure;
        Debug.Log("Pick " + self.name);

        if (self.currentDirection == Vector3.zero)
        {
            self.currentDirection = self.PickRandomMoveDirection();
        }
        return BTState.Success;
    }
}