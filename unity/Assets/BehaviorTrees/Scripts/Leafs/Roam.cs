﻿using UnityEngine;
using AillieoUtils.EasyBehaviorTree;
using System;
using BehaviorTrees;

[Serializable]
public class Roam : NodeAction
{
    public override void Cleanup()
    {
    }

    protected override BTState ExecuteTask(float deltaTime)
    {
        Organism self = behaviorTree.blackBoard[BlackboardKeys.SELF] as Organism;
        if (self == null) return BTState.Failure;

        Debug.Log("Roam " + self.name);
        
        if (self.currentDirection == Vector3.zero)
        {
            return BTState.Failure;
        }

        Vector3 dir = self.currentDirection - self.transform.position;
        if (dir.sqrMagnitude > 0)
        {
            self.transform.forward = dir;
        }

        dir.Normalize();
        Vector3 move = dir * self.speed * deltaTime;

        if (Vector3.Distance(self.transform.position, self.currentDirection) < 1)
        {
            self.currentDirection = Vector3.zero;
        }
        else
        {
            self.transform.position += move;
        }

        return BTState.Success;
    }
}