using UnityEngine;
using AillieoUtils.EasyBehaviorTree;
using System;
using BehaviorTrees;

[Serializable]
public class MoveTowardsTarget : NodeAction
{
    public override void Cleanup()
    {
    }

    protected override BTState ExecuteTask(float deltaTime)
    {
        Organism self = behaviorTree.blackBoard[BlackboardKeys.SELF] as Organism;
        Organism target = behaviorTree.blackBoard?[BlackboardKeys.NEARBY_ORGANISM] as Organism;

        if (self == null || target == null) return BTState.Failure;
        Debug.Log("Move towards target " + self.name);

        Vector3 dir = target.transform.position - self.transform.position;
        if (dir.sqrMagnitude > 0)
        {
            self.transform.forward = dir;
        }

        dir.Normalize();
        Vector3 move = dir * self.speed * deltaTime;

        self.transform.position += move;

        return BTState.Success;
    }
}