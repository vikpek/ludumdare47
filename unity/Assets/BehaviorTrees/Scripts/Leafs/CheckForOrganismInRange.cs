﻿using System;
using AillieoUtils.EasyBehaviorTree;
using BehaviorTrees;
using UnityEngine;

[Serializable]
public class CheckForOrganismInRange : NodeAction
{
    public override void Cleanup()
    {
    }

    protected override BTState ExecuteTask(float deltaTime)
    {
        Organism self = behaviorTree.blackBoard[BlackboardKeys.SELF] as Organism;
        if (self == null) return BTState.Failure;
        
        Debug.Log("Check if Target is in range " + self.name);
        if (behaviorTree.blackBoard[BlackboardKeys.NEARBY_ORGANISM] != null) return BTState.Success;
        
        if (self.GetNearbyOrganism(out Organism nearbyOrganism))
        {
            behaviorTree.blackBoard[BlackboardKeys.NEARBY_ORGANISM] = nearbyOrganism;
        }
        else
        {
            behaviorTree.blackBoard[BlackboardKeys.NEARBY_ORGANISM] = null;
        }

        return BTState.Success;
    }
}