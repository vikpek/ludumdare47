﻿namespace BehaviorTrees
{
    public static class BlackboardKeys
    {
        public static string SELF = "self";
        public static string NEARBY_ORGANISM = "nearby_organism";
    }
}