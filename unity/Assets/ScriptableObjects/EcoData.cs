﻿using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = nameof(EcoData), menuName = "Eco/" + nameof(EcoData))]
    public class EcoData : ScriptableObject
    {
        public List<Organism> organisms;

        public int durationDay = 5;
        public int durationNight = 5;
        public int nightHealthLoss = 300;

        public Daytime daytime = Daytime.Day;
    }

    public enum Daytime
    {
        Day,
        Night
    }
}