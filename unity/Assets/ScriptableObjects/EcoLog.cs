﻿using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = nameof(EcoLog), menuName = "Eco/" + nameof(EcoLog))]
    public class EcoLog : ScriptableObject
    {
        public List<string> ecoMessages = default;
        public List<float> ecoOxygen = default;
    }
}