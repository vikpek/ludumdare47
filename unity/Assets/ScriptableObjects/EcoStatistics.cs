﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = nameof(EcoStatistics), menuName = "Eco/" + nameof(EcoStatistics))]
    public class EcoStatistics : ScriptableObject
    {
        public int countPlants = 0;
        public int countMushrooms = 0;
        public int countBirdsOfPrey = 0;
        public int countMouses = 0;
    }
}